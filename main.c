#define F_CPU 1000000UL

#define BLINK_DELAY 500U

#include <avr/io.h>
#include <util/delay.h>


static void chain_blink(volatile uint8_t *PORTX, uint8_t start_id, uint8_t end_id) {
	for (uint8_t i = start_id; i <= end_id; i++) {
		*PORTX = (uint8_t)_BV(i);
		_delay_ms(BLINK_DELAY);
	}
	*PORTX = !_BV(end_id);
}


int main (void) {
	DDRD = 0xFF;
	DDRB = _BV(PORTD6) | _BV(PORTD7);

	while (1) {
		chain_blink(&PORTD, 0, 7);
		chain_blink(&PORTB, 6, 7);
	};

	return 0;
} 
